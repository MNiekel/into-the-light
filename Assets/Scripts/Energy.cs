﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour {

	public Transform sun;

	float chargeMultiplier = 100f;
	float optimalAngle = 25f;
	float cutOffAngle = 85f;

	private void Update() {
		CalculateAngle();
	}

	void CalculateAngle() {
		if (Physics.Raycast(transform.position + transform.up, -sun.forward)) {
			//print("Sun blocked");
			Battery.Charge(0f);
		} else {
			Vector3 dir = sun.position - (transform.position + transform.up);

			float angle = Vector3.Angle(dir, transform.forward);

			if (angle > cutOffAngle) {
				//print("Panel not aimed at sun, not charging.");
				Battery.Charge(0f);
			} else {
				//print("Charge angle = " + angle);
				if (angle < optimalAngle) {
					Battery.Charge(Time.deltaTime * chargeMultiplier / optimalAngle);
				} else {
					Battery.Charge(Time.deltaTime * chargeMultiplier / angle);
				}
			}
		}
	}
}
