﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour {

	[SerializeField] float dayTime;	
	void Update () {
		transform.RotateAround(Vector3.zero, transform.right, 360 / dayTime * Time.deltaTime);
	}
}
