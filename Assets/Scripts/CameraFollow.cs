﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	[SerializeField] Transform target;
	[SerializeField] Vector3 offset;
	[SerializeField] float step;

	void FixedUpdate () {
		Vector3 targetPos = target.position;
		targetPos += target.forward * offset.z + target.right * offset.x + target.up * offset.y;
		Vector3 currentPos = transform.position;
		transform.position = Vector3.MoveTowards(currentPos, targetPos, step * Time.deltaTime);
		transform.LookAt(target, transform.position.normalized);
	}
}
