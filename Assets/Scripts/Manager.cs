﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

	int crystals = 0;
	[SerializeField] int goalCrystals = 15;
	AudioSource pling;

	UIController uiController;
	public Transform crystalHeap;
	FillCart fillScript;
	int animateInterval;


	bool allCollected = false;

	private void Start() {
		pling = GetComponent<AudioSource>();
		fillScript = crystalHeap.GetComponent<FillCart>();
		goalCrystals /= fillScript.Frames;
		goalCrystals *= fillScript.Frames;
		animateInterval = goalCrystals / fillScript.Frames;
		uiController = GameObject.Find("UI").GetComponent<UIController>();
		uiController.UpdateCrystalCounter(crystals, goalCrystals);
	}

	private void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
	}

	public void Crystal(){
		if (allCollected) return;
		crystals += 1;
		if (((int) crystals) % (int) animateInterval == 0) {
			fillScript.PlayFrame();
		}
		pling.Play();
		uiController.UpdateCrystalCounter(crystals, goalCrystals);
		print (crystals);
		if (crystals >= goalCrystals){
			uiController.PopupMessage(true);
			allCollected = true;
			GameObject entrance = GameObject.FindGameObjectWithTag("Entrance");
			entrance.SetActive(false);
			print ("Locate Station!");
		}
	}

	public void Rocket() {
		if (allCollected){
			print ("You won!!!");
			uiController.EndMessage(true);
		}else{
			uiController.PopupMessage(false);
			print ("Collect " + (goalCrystals - crystals).ToString() + " more crystals");
		}
	}
}
