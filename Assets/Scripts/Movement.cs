﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	[SerializeField] float maxSpeed;
	[SerializeField] float wheelRot;
	[SerializeField] float batteryUse = 0f;

	Manager manager;

	
	Rigidbody rb;
	
	float steer = 0f;
	float forward = 0f;

	void Start(){
		rb = GetComponent<Rigidbody>();
		manager = GameObject.Find("Manager").GetComponent<Manager>();
		Battery.Initialize();
	}

	void Update() {
		steer = Input.GetAxisRaw("Horizontal");
		forward = Input.GetAxisRaw("Vertical");
	}

	void FixedUpdate () {
		if (Battery.EnergyLevel <= 0) return; // cannot move when battery is drained
		Vector3 move = transform.forward;
		move += transform.right * wheelRot * steer * forward;
		if (forward != 0)	{
			transform.LookAt(move + transform.position, transform.up);
			Battery.Drain(batteryUse * Time.deltaTime);
		}
		move = transform.forward * maxSpeed * forward * Time.deltaTime;
		
		rb.position += move;
	}

	void OnTriggerEnter(Collider other) {
		print("Triggered by: " +other.tag);
		if (other.CompareTag("Crystal")) {
			other.gameObject.SetActive(false);
			manager.Crystal();
			Destroy(other.gameObject);
		}
		if (other.CompareTag("Finish")) {
			other.gameObject.SetActive(false);
			manager.Rocket();
		}
	}

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.CompareTag("Entrance")) {
			manager.Rocket();
		}
	}
}
