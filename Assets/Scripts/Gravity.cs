﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Gravity : MonoBehaviour {

	public float gravityConstant = -9.83f;
	Rigidbody rb;

	private void Awake() {
		rb = GetComponent<Rigidbody>();
	}

	private void Start() {
		rb.useGravity = true;
	}

	void FixedUpdate () {
		Physics.gravity = transform.position.normalized * gravityConstant * rb.mass / 1000f;
	}
}
