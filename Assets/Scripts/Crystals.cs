﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystals : MonoBehaviour {

	public GameObject[] crystals;

	float distanceFromOrigin = 1000f;
	[SerializeField] int numberOfCrystals = 50;

	void Start () {
		for (int i = 0; i < numberOfCrystals; i++) {
			SpawnCrystal();
		}
	}

	void SpawnCrystal() {
		Vector3 dir = Random.insideUnitSphere;
		if (dir == Vector3.zero) dir = Vector3.one;
		dir = dir.normalized * distanceFromOrigin;  // point outside world
		RaycastHit hit;
		if (Physics.Raycast(dir, -dir, out hit, distanceFromOrigin)) {
			if (hit.transform.CompareTag("World")) {
				Vector3 pos = hit.point;
				Vector3 normal = hit.normal;
				GameObject crystal = GameObject.Instantiate(crystals[Random.Range(0, crystals.Length)]);
				crystal.transform.position = pos;
				crystal.transform.rotation = Quaternion.FromToRotation(crystal.transform.forward, normal);
			}
		} else {
			print("Did not find world");
		}
	}
}
