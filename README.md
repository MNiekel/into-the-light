# Into the Light

This game was created for [Brackeys gamejam](https://itch.io/jam/brackeys).
Within a week, while working and school took up most of the time.

## Gameplay

The goal is to find all the crystals and bring them back to your spaceship.
Your electrical planet-buggy has a battery, but you need the solar-panels
to keep the batteries filled.

## Interface

Use the normal 'WASD' to steer the vehicle. Going backwards is not 
really supported, but given the dents, it has been done before.

If your battery goes empty, wait for daylight, and continue your journey.
