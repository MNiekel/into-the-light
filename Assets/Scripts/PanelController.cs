﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour {

	public Transform panel;
	public Transform sun;
	float tilt = 0f;
	float rotate = 0f;
	[SerializeField] float tiltSpeed = 20f;
	[SerializeField] float minTilt = 20f;
	[SerializeField] float maxTilt = 85f;
	[SerializeField] float rotateSpeed = 30f;
	void Start () {
		Vector3 angles = panel.localEulerAngles;
		angles.y = 0f;
		angles.z = 0f;
		panel.localEulerAngles = angles;
	}
	
	// Update is called once per frame
	void Update () {
		tilt = Input.GetAxis("Mouse ScrollWheel");
		rotate = Input.GetAxis("Mouse X");
	}

	void FixedUpdate() {
		if (tilt != 0) {
			panel.RotateAround(panel.position, panel.right, tilt*Time.deltaTime*tiltSpeed);
			if (panel.localEulerAngles.x < minTilt) {
				Vector3 angles = new Vector3(minTilt, 0f, 0f);
				panel.localEulerAngles = angles;
			} else {
				if (panel.localEulerAngles.x > maxTilt) {
					Vector3 angles = new Vector3(maxTilt, 0f, 0f);
					panel.localEulerAngles = angles;
				}
			}
		}
		if (rotate != 0) {
			transform.RotateAround(transform.position, transform.forward, rotate*Time.deltaTime*rotateSpeed);
		}
	}
}
