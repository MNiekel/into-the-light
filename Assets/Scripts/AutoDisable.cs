﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisable : MonoBehaviour {

	[SerializeField] float delay = 2.5f;

	void OnEnable() {
        Invoke("Disable", delay);
    }

    void Disable() {
		gameObject.SetActive(false);
	}
}
