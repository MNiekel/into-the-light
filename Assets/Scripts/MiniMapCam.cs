﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapCam : MonoBehaviour {

	public Transform player;

	private void FixedUpdate() {
		Vector3 pos = player.position.normalized * 280;
		transform.position = pos;
		transform.LookAt(Vector3.zero, -transform.forward);
	}
}
