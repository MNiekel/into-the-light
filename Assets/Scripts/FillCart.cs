﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillCart : MonoBehaviour {

	Animator anim;
	int numAnimations = 5;

	public int Frames {
		get { return numAnimations; }
	}

	private void Awake() {
		anim = GetComponent<Animator>();
	}

	private void Start() {
		anim.speed = 0;
		anim.Play("Crystals_Grow");
	}

	public void PlayFrame() {
		Invoke("StopAnimation", 1f);
		anim.speed = 1;
	}

	void StopAnimation() {
		anim.speed = 0;
	}
}
