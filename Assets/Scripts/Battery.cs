﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Battery {

	static float maxEnergyLevel = 100f;  // kWh
	static float energyLevel; // percentage
	static float chargingLevel;

	public static float EnergyLevel {
		get { return energyLevel; }
		set { energyLevel = value; }
	}

	public static float ChargingLevel {
		get { return chargingLevel; }
	}

	public static void Initialize() {
		energyLevel = 100f;
	}

	public static void Charge(float amount) {  // amount in kWh per frame
		chargingLevel = 100f * amount / maxEnergyLevel / Time.deltaTime;  // percentage of battery per second
		energyLevel += chargingLevel * Time.deltaTime;
		if (energyLevel > 100) energyLevel = 100f;
	}

	public static void Drain(float amount) {
		energyLevel -= 100f * amount / maxEnergyLevel;
		if (energyLevel < 0) {
			energyLevel = 0f;
		}
	}
}
