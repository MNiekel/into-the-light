﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public GameObject batteryStatus;
	public GameObject chargingStatus;
	public Text crystalsText;
	public GameObject startMessage;
	public GameObject endMessage;
	public GameObject popupMessage1;
	public GameObject popupMessage2;

	RawImage[] batteryParts;
	Image[] flashes;
	float partPercentage;

	private void Start() {
		batteryParts = batteryStatus.GetComponentsInChildren<RawImage>();
		flashes = chargingStatus.GetComponentsInChildren<Image>();
		partPercentage = 100f / batteryParts.Length;
		UpdateBatteryStatus();
		UpdateChargingStatus();
	}
	
	private void Update() {
		UpdateBatteryStatus();
		UpdateChargingStatus();
	}

	void UpdateBatteryStatus() {
		float battery = Battery.EnergyLevel;
		Color col = Color.green;
		if (battery <= partPercentage) {
			col = Color.red;
		} else {
			if (battery <= 50) {
				col = Color.yellow;
			}
		}
		for (int i = 0; i < batteryParts.Length; i++) {
			if (battery >= i * partPercentage) {			
				if (battery <= 0.5f) {
					batteryParts[i].color = Color.black;
				} else {
					batteryParts[i].color = col;
				}
			} else {
				batteryParts[i].color = Color.black;
			}
		}
	}

	void UpdateChargingStatus() {
		float charging = Battery.ChargingLevel;  // in battery-percentage / second
		for (int i = 0; i < flashes.Length; i++) {
			if (charging > i * 1.2f) {
				flashes[i].enabled = true;
			} else {
				flashes[i].enabled = false;
			}
		}
	}

	public void UpdateCrystalCounter(int amount, int goal) {
		crystalsText.text = amount.ToString() + " / " + goal.ToString();
	}

	public void StartMessage(bool enabled = false) {
		startMessage.SetActive(enabled);
	}

	public void EndMessage(bool enabled = false) {
		endMessage.SetActive(enabled);
	}

	public void PopupMessage(bool allCrystals) {
		if (allCrystals) {
			popupMessage1.SetActive(true);
		} else {
			popupMessage2.SetActive(true);
		}
	}
}
